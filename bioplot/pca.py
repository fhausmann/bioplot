"""Functions for PCA with annotation."""
import contextlib
from typing import Any, Optional, Union

import pandas as pd
import seaborn as sns
from sklearn.decomposition import PCA as skPCA
from typing_extensions import TypeAlias

PandasObject: TypeAlias = Union[pd.Series, pd.DataFrame]


class PCAResult:
    """Container storing results from PCA."""

    def __init__(
        self,
        pca_object: "PCA",
        transformed_data: pd.DataFrame,
        metadata: Optional[pd.DataFrame] = None,
    ):
        """Initialize the object.

        Args:
        ----
            pca_object (PCA): A fitted PCA object
            transformed_data (pd.DataFrame): The transformed input data, which are the results.
            metadata (Optional[pd.DataFrame], optional): Metadata, which can be used for plotting. Defaults to None.
        """
        self._transformed_data = transformed_data
        self._merged = transformed_data
        if metadata is not None:
            self._merged = pd.merge(
                self._transformed_data,
                metadata,
                left_index=True,
                right_index=True,
            )
        self._pca = pca_object
        self.explained_variance_ratio_ = pd.Series(
            self._pca.explained_variance_ratio_,
            index=transformed_data.columns,
            name="explained_variance_ratio",
        )

    def plot(
        self,
        x: str = "PCA_1",
        y: str = "PCA_2",
        hue: Optional[str] = None,
        style: Optional[str] = None,
        **kwargs,
    ) -> sns.FacetGrid:
        """Plot specific PCs with optional metadata.

        Args:
        ----
            x (str, optional): The first principle component to plot. Defaults to "PCA_1".
            y (str, optional):  The second principle component to plot. Defaults to "PCA_2".
            hue (Optional[str], optional): The color for the plot. Defaults to None.
            style (Optional[str], optional): The style for the points. Defaults to None.
            **kwargs: Keyword arguments for seaborn.relplot.

        Returns:
        -------
            sns.FacetGrid: the plot.
        """
        g = sns.relplot(x=x, y=y, hue=hue, style=style, data=self._merged)
        g.set_xlabels(f"{x} ({self.explained_variance_ratio_[x]*100:.2f}%)")
        g.set_ylabels(f"{y} ({self.explained_variance_ratio_[y]*100:.2f}%)")
        for ax in g.axes.flat:
            ax.tick_params(left=False, bottom=False)
            ax.set(yticklabels=[], xticklabels=[])
        return g

    def to_df(self) -> pd.DataFrame:
        """Convert the results as a table.

        Returns
        -------
            pd.DataFrame: The transformed results.
        """
        return self._transformed_data.copy()

    def plot_variance_ratio(
        self, highlight_selection_point: Optional[float] = 95, **kwargs
    ) -> sns.FacetGrid:
        """Plot a variance ratio plot.

        Args:
        ----
            highlight_selection_point (Optional[float], optional): highlight the
            point where at least `highlight_selection_point` percent
            variance is covered. Defaults to 95.
            **kwargs: Keyword arguments for seaborn.relplot.

        Raises:
        ------
            RuntimeError: In case the function is called on an unfitted PCA object.



        Returns:
        -------
            sns.FacetGrid: the plot.
        """
        return self._pca.plot_variance_ratio(
            highlight_selection_point=highlight_selection_point, **kwargs
        )


class PCA(skPCA):
    """Extented sklearn.decomposition.PCA class."""

    def __init__(self, *args, metadata: Optional[PandasObject] = None, **kwargs):
        """Initialize the PCA object using arguments from sklearn.decomposition.PCA.

        Args:
        ----
            *args: Positional arguments for sklearn.decomposition.PCA.
            metadata (Optional[PandasObject], optional): Optional metadata for plotting.
                Defaults to None.
            **kwargs: Keyword arguments for sklearn.decomposition.PCA.
        """
        super().__init__(*args, **kwargs)
        if metadata is not None:
            with contextlib.suppress(AttributeError):
                metadata = metadata.to_frame()
        self._metadata = metadata

        self.explained_variance_ratio = None

    def fit(self, X: Any) -> "PCA":
        """Fit the PCA.

        Args:
        ----
            X (Any): Input data

        Returns:
        -------
            PCA: the fitted PCA object.
        """
        result = super().fit(X)
        self.explained_variance_ratio = pd.Series(
            self.explained_variance_ratio_,
            index=[
                f"PCA_{i}" for i in range(1, len(self.explained_variance_ratio_) + 1)
            ],
            name="explained_variance_ratio",
        )
        return result

    def transform(self, X: pd.DataFrame) -> PCAResult:
        """Transform data as using fitted PCA.

        Args:
        ----
            X (Any): Input for transform.

        Returns:
        -------
            PCAResult: The PCAResult object with transformed data.
        """
        results = pd.DataFrame(
            super().transform(X),
            index=X.index,
            columns=self.explained_variance_ratio.index,
        )
        return PCAResult(
            pca_object=self,
            transformed_data=results,
            metadata=self._metadata,
        )

    def plot_variance_ratio(
        self, highlight_selection_point: Optional[float] = 95, **kwargs
    ) -> sns.FacetGrid:
        """Plot a variance ratio plot.

        Args:
        ----
            highlight_selection_point (Optional[float], optional): highlight the
            point where at least `highlight_selection_point` percent
            variance is covered. Defaults to 95.
            **kwargs: Keyword arguments for seaborn.relplot.

        Raises:
        ------
            RuntimeError: In case the function is called on an unfitted PCA object.



        Returns:
        -------
            sns.FacetGrid: the plot.
        """
        if self.explained_variance_ratio is None:
            raise RuntimeError("PCA object needs to be fitted first!")
        data = self.explained_variance_ratio.to_frame() * 100
        data["N_PC"] = range(1, len(data) + 1)
        data["cumsum"] = data.explained_variance_ratio.cumsum()
        g = sns.relplot(x="N_PC", y="explained_variance_ratio", data=data, **kwargs)
        g.set_xlabels("Number of principle components")
        g.set_ylabels("Explained variance [%]")
        if highlight_selection_point is not None:
            selection_point = data.loc[
                data["cumsum"] > highlight_selection_point, "N_PC"
            ].min()
            for ax in g.axes.flat:
                ax.axvline(selection_point, c="lightgray", alpha=0.5)
        return g

    def __repr__(self) -> str:
        """Return the string representation.

        Returns
        -------
            str: The string representation.
        """
        try:
            return super().__repr__()
        except RuntimeError:
            return f"{self.__class__}"
