"""FDR calibration based on the R package CP4P."""

from typing import Any, Dict

import numpy as np
import scipy
import seaborn as sns


def _compute_difference(x: np.ndarray, y1: np.ndarray, y2: np.ndarray) -> float:
    z = y1 - y2
    dx = x[1:] - x[:-1]
    cross_test = np.sign(z[:-1] * z[1:])
    dx_intersect = -dx / (z[1:] - z[:-1]) * z[:-1]
    areas_pos = abs(z[:-1] + z[1:]) * 0.5 * dx
    areas_neg = 0.5 * dx_intersect * abs(z[:-1]) + 0.5 * (dx - dx_intersect) * abs(
        z[1:]
    )
    areas = np.where(cross_test < 0, areas_neg, areas_pos)
    return np.sum(areas)


def fdr_calibration(pvalues: np.ndarray, with_plot: bool = True) -> Dict[str, Any]:
    """Compute FDR calibration stats and optionally a calibration plot.

    pvalues (np.ndarray): uncorrected p-values.
    with_plot (bool): Also display the calibration plot. Defaults to True.

    """
    pi0 = min(1, 2 * np.mean(pvalues))
    x = np.arange(0, 1, 0.001)
    y = scipy.stats.ecdf(1 - pvalues).cdf.evaluate(x)
    yp0 = x * pi0
    difference = y - yp0
    i = len(difference)
    while difference[i - 1] > 0:
        i -= 1
    remaining = _compute_difference(x[i:], y[i:], yp0[i:])
    auc = ((1 - pi0) / 2 - remaining) / ((1 - pi0) / 2)
    auc2 = _compute_difference(x[:i], y[:i], yp0[:i])
    results = {
        "pi0": pi0,
        "differential concentration": (auc if not np.isnan(auc) else 0),
        "uniformity underestimation": auc2,
    }
    if not with_plot:
        return results

    ax = sns.lineplot(x=x, y=y, color="black")
    ax.plot(x, x * pi0, color="gray")
    pos_only = (y[:i] - yp0[:i])  > 0
    ax.fill_between(x[:i][pos_only], y[:i][pos_only], yp0[:i][pos_only], color="red", alpha=0.2)
    ax.fill_between(x[i:], y[i:], yp0[i:], color="green", alpha=0.2)
    ymax = ax.get_yticks()[-2]
    xmin = ax.get_xticks()[1]
    ax.text(
        xmin,
        ymax,
        f"Non differential proportion: {results['pi0']*100:.1f}%"
        + f"\nDifferential concentration: {results['differential concentration']*100:.1f}%"
        + f"\nUniformity underestimation: {results['uniformity underestimation']:.3f}",
        verticalalignment="top",
    )
    results["ax"] = ax
    return results
