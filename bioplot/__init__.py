"""Python tools for plotting in bioinformatics."""

__version__ = "0.1.1"
from bioplot.clustermap_with_annotation import clustermap_with_annotation as clustermap
from bioplot.fdr_calibration import fdr_calibration
from bioplot.pca import PCA
from bioplot.volcano import volcanoplot

__all__ = ["clustermap", "fdr_calibration", "volcanoplot", "PCA"]
