"""Volcano plot function."""
from typing import Any, Dict, List, Optional, Union

import numpy as np
import pandas as pd
import seaborn as sns
from adjustText import adjust_text


def volcanoplot(
    log2foldchanges: str,
    pvalues: str,
    data: pd.DataFrame,
    pvalue_threshold: float = 0.05,
    log2foldchange_threshold: float = 0.585,
    size: Optional[str] = "-log10p",
    colors: Optional[Dict[str, str]] = {
        "downregulated": "blue",
        "upregulated": "red",
        "non-significant": "lightgray",
    },
    highlight: Optional[Union[List, str]] = "deregulated",
    clip_pvalue: float = -1,
    annotation_kwargs: Optional[Dict[str, Any]] = None,
    **kwargs,
) -> sns.FacetGrid:
    """Create a volcano plot.

    Args:
    ----
        log2foldchanges (str): Column name of the x-axis.
        pvalues (str): Column name of the y-axis (will be -log10 transformed)
        data (pd.DataFrame): Dataframe
        pvalue_threshold (float, optional): Threshold for p-values.
            Used to calculate significance and horizonal line in the plot.
            Defaults to 0.05.
        log2foldchange_threshold (float, optional): Threshold for log2foldchanges.
            Used to calculate significance and vertical lines in the plot. Defaults to 0.585.
        size (str, optional): Adjust size of the dots. Defaults to "-log10p".
        colors (Dict[str,str], optional): Colormapping of the dots. Defaults to { "downregulated": "blue", "upregulated": "red", "non-significant": "lightgray", }.
        highlight (Optional[Union[List, str]], optional): Points to label with IDs. Defaults to "deregulated".
        clip_pvalue (float, optional): Clip pvalues to this lower limit. Defaults to '-1' meaning, lowest pvalue greater 0.0."
        annotation_kwargs (Optional[Dict[str, Any]], optional): Further arguments for labeling. Defaults to None.
        kwargs: Arguments passed to seaborn.relplot.

    Returns:
    -------
        sns.FacetGrid: The volcanoplot figure.
    """
    if annotation_kwargs is None:
        annotation_kwargs = {}
    if "hue" in kwargs:
        raise RuntimeError("Setting hue is not supported.")
    if size == "-log10p":
        size = "_-log10p"
    data = data.copy()
    data["_-log10p"] = -np.log10(data[pvalues])
    if clip_pvalue < 0:
        clip_pvalue = data["_-log10p"][np.isfinite(data["_-log10p"])].max()
    else:
        clip_pvalue = -np.log10(clip_pvalue)
    data["_-log10p"] = np.clip(data["_-log10p"], None, clip_pvalue)
    data["_colormap"] = "non-significant"
    data.loc[
        (data[log2foldchanges] < -log2foldchange_threshold)
        & (data[pvalues] < pvalue_threshold),
        "_colormap",
    ] = "downregulated"
    data.loc[
        (data[log2foldchanges] > log2foldchange_threshold)
        & (data[pvalues] < pvalue_threshold),
        "_colormap",
    ] = "upregulated"
    data["_to_highlight"] = False
    if highlight == "deregulated":
        data.loc[data["_colormap"] != "non-significant", "_to_highlight"] = True
    elif highlight in data["_colormap"].unique():
        data.loc[data["_colormap"] == highlight, "_to_highlight"] = True
    elif (
        not isinstance(highlight, str) and highlight is not None and len(highlight) > 1
    ):
        data.loc[list(highlight), "_to_highlight"] = True
    elif highlight is not None:
        raise ValueError(
            f"Could not interpret value '{highlight}' for parameter 'highlight'"
        )
    kwargs.setdefault("legend", False)
    g = sns.relplot(
        x=log2foldchanges,
        y="_-log10p",
        data=data,
        sizes=data[size],
        hue="_colormap" if colors is not None else None,
        palette=colors if colors is not None else None,
        **kwargs,
    )
    g.set_xlabels("$log_2$FoldChange")
    g.set_ylabels("$-log_{10}$(p-value)")
    for ax in g.axes.flat:
        ax.axhline(-np.log10(pvalue_threshold), color="lightgray", linestyle="--")
        ax.axvline(-log2foldchange_threshold, color="lightgray", linestyle="--")
        ax.axvline(log2foldchange_threshold, color="lightgray", linestyle="--")
    for idx, group in g.facet_data():
        groupsub = group.query("_colormap != 'non-significant'")
        ax = g.axes[idx[0:2]]
        texts = [
            ax.annotate(
                name, (row[log2foldchanges], row["_-log10p"]), **annotation_kwargs
            )
            for name, row in groupsub.iterrows()
        ]
        if texts:
            adjust_text(texts, ax=ax)
    return g
