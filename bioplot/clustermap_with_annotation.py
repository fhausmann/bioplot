"""Plotting functions for clustermaps with annotation."""
from typing import Any, Dict, Optional, Tuple, Union

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.text as mtext
import pandas as pd
import seaborn as sns
from matplotlib.cm import ScalarMappable
from matplotlib.colors import Normalize
from matplotlib.legend_handler import HandlerBase
from matplotlib.patches import Patch
from typing_extensions import TypeAlias

PandasObject: TypeAlias = Union[pd.Series, pd.DataFrame]


def _transform_categorical_annotation(
    annotation: pd.DataFrame, cmap: Optional[Union[str, Dict[Any, Any]]]
) -> Tuple[PandasObject, Dict[str, Dict[Any, Any]]]:
    if annotation.empty:
        return pd.DataFrame(), {}
    new_cmap = {}
    colors = pd.DataFrame(index=annotation.index, columns=annotation.columns)
    for column in annotation.columns:
        unique = set(annotation[column].values)
        if cmap is None:
            new_cmap[column] = dict(
                zip(unique, sns.color_palette(n_colors=len(unique)))
            )
            colors[column] = annotation[column].map(new_cmap[column].get)
        elif isinstance(cmap, str):
            new_cmap[column] = dict(zip(unique, sns.color_palette(cmap)))
            colors[column] = annotation[column].map(new_cmap[column].get)
        else:
            new_colors, tmp = _transform_categorical_annotation(
                annotation[[column]], cmap.get(column, None)
            )
            colors[column] = new_colors.squeeze()
            new_cmap.update(tmp)
    return colors, new_cmap


def _transform_numerical_annotation(
    annotation: pd.DataFrame, cmap: Optional[Union[str, Dict[Any, Any]]]
) -> Tuple[PandasObject, Dict[Any, Any]]:
    if annotation.empty:
        return pd.DataFrame(), {}
    if cmap is None:
        cmap = {}
    default = "Blues"
    if isinstance(cmap, str):
        default = cmap
        cmap = {}
    colors = pd.DataFrame(index=annotation.index, columns=annotation.columns)
    colorgradients = {}
    for column in annotation.columns:
        mins, maxs = annotation[column].describe()[["min", "max"]].values
        if mins < 0 and maxs > 0:
            min_max = max(abs(mins), maxs)
            mins = -min_max
            maxs = min_max
        norm = Normalize(mins, maxs)
        mappable = ScalarMappable(norm=norm, cmap=cmap.get(column, default))
        colors[column] = list(mappable.to_rgba(annotation[column])[:, :-1])
        colorgradients[column] = mappable
    return colors, colorgradients


def _transform_annotation(
    annotation: PandasObject, cmap: Optional[Union[str, Dict[Any, Any]]] = None
) -> Tuple[PandasObject, Dict[Any, Any], Dict[str, ScalarMappable]]:
    if isinstance(annotation, pd.Series):
        annotation = annotation.to_frame()
    numeric_colors, colorgradients = _transform_numerical_annotation(
        annotation.select_dtypes(include="number"), cmap
    )
    categorical_colors, categorical_cmap = _transform_categorical_annotation(
        annotation.loc[:, ~annotation.columns.isin(numeric_colors.columns)], cmap
    )
    colors = pd.concat((numeric_colors, categorical_colors), axis=1)[annotation.columns]
    return colors, categorical_cmap, colorgradients


class LegendTitle(HandlerBase):
    """Legend title to be added to legends."""

    def __init__(self, text_props: Optional[Dict[Any, Any]] = None):
        """Create a LegendTitle object.

        Args:
        ----
            text_props (Optional[Dict[Any, Any]], optional): Some further formatting arguments. Defaults to None.
        """
        self.text_props = text_props or {}
        super().__init__()

    def create_artists(
        self, legend, orig_handle, xdescent, ydescent, width, height, fontsize, trans
    ):
        """Create an artist from LegendTitle. Should not be used."""
        # pylint: disable=too-many-arguments
        raise RuntimeError("This function is not needed usually.")

    def legend_artist(self, legend, orig_handle, fontsize, handlebox) -> mtext.Text:
        """Add the legendtitle artist.

        Args:
        ----
            legend: Unused argument for compatibility reasons.
            orig_handle: Unused argument for compatibility reasons.
            fontsize: Textsize.
            handlebox: Handlebox for the artist.

        Returns:
        -------
            mtext.Text: The legend title.
        """
        x0, y0 = handlebox.xdescent, handlebox.ydescent
        title = mtext.Text(x0, y0, orig_handle, **self.text_props)
        handlebox.add_artist(title)
        return title


def _create_legend(
    annotation: PandasObject,
    cmap: Dict[Any, Any],
    position: Tuple[float, float],
    ax: matplotlib.axes.Axes,
) -> matplotlib.legend.Legend:
    handles = []
    labels = []
    if not isinstance(annotation, pd.DataFrame):
        annotation = annotation.to_frame()
    for column in cmap:
        values = annotation[column].unique()  # type: ignore
        handles.append(column)
        handles += [Patch(facecolor=cmap[column][name]) for name in values]
        labels.append("")
        labels += list(values)
    leg = ax.legend(
        handles,
        labels,
        bbox_to_anchor=position,
        bbox_transform=plt.gcf().transFigure,
        handler_map={str: LegendTitle()},
        loc="upper right",
    )
    return leg


def clustermap_with_annotation(
    data: pd.DataFrame,
    row_annotation: Optional[PandasObject] = None,
    col_annotation: Optional[PandasObject] = None,
    row_cmap: Optional[Union[str, Dict[Any, Any]]] = None,
    col_cmap: Optional[Union[str, Dict[Any, Any]]] = None,
    row_legend_position: Tuple[float, float] = (0.2, 0.2),
    col_legend_position: Tuple[float, float] = (1.1, 1.1),
    **kwargs,
) -> sns.matrix.ClusterGrid:
    """Plot a seaborn clustermap with row & column annotation.

    Args:
    ----
        data: (pd.DataFrame):
            Dataframe with the data to plot.
        row_annotation (PandasObject, optional):
            Dataframe with annotation for rows. Defaults to None.
        col_annotation (PandasObject, optional):
            Dataframe with annotation for columns. Defaults to None.
        row_cmap (Union[str, Dict[Any, Any]], optional):
            Colormap with row annotations.
            Defaults to None, which means automatic selection.
        col_cmap (Union[str, Dict[Any, Any]], optional):
            Colormap with row annotations.
            Defaults to None, which means automatic selection.
        row_legend_position (Tuple[float, float], optional):
            Position of the row legend. Defaults to (0.2, 0.2).
        col_legend_position (Tuple[float, float], optional):
            Position of the column legend. Defaults to (1.1, 1.1).
        kwargs: All arguments which can be used with seaborn.clustermap.

    Returns:
    -------
        sns.matrix.ClusterGrid: Clustergrid object with annotation.
    """
    # pylint: disable=too-many-arguments, too-many-locals
    row_colors = None
    col_colors = None
    row_cmap_to_use: Dict[str, Dict[Any, Any]] = {}
    col_cmap_to_use: Dict[str, Dict[Any, Any]] = {}
    col_gradients = None
    row_gradients = None
    if row_annotation is not None:
        row_colors, row_cmap_to_use, row_gradients = _transform_annotation(
            row_annotation, row_cmap
        )
    if col_annotation is not None:
        col_colors, col_cmap_to_use, col_gradients = _transform_annotation(
            col_annotation, col_cmap
        )
    g = sns.clustermap(data, col_colors=col_colors, row_colors=row_colors, **kwargs)

    if row_cmap_to_use:
        g.ax_row_colors.add_artist(
            _create_legend(
                row_annotation, row_cmap_to_use, row_legend_position, g.ax_row_colors
            )
        )
    if col_cmap_to_use:
        g.ax_col_colors.add_artist(
            _create_legend(
                col_annotation, col_cmap_to_use, col_legend_position, g.ax_col_colors
            )
        )
    if row_gradients:
        for column, mappable in row_gradients.items():
            plt.colorbar(
                mappable,
                label=column,
                shrink=0.25,
                ax=g.ax_row_dendrogram,
                location="left",
                pad=0.3,
            )
    if col_gradients:
        for column, mappable in col_gradients.items():
            plt.colorbar(
                mappable,
                label=column,
                shrink=0.25,
                ax=g.ax_col_dendrogram,
                location="left",
                pad=0.3,
            )
    return g
